import engine.eandata
import engine.colruyt
import domain.product

def searchEanCode(eanCode):
	product = engine.eandata.search(eanCode)
	return product

def searchColruyt(eanCode):
	try:
		api = engine.colruyt.ColruytAPI()
		item = api.search(eanCode)
		api.logout()
		del(api)
		return item
	except ValueError as ve:
		print ve
	except:
		"Unexpected error:", sys.exc_info()[0]