class Product:

	def __init__(self):
		self._name = None
		self._description = None
		self._brand = None
		self._category = None

	def __str__(self):
		return "[%s]\n Brand: %s\n Name: %s\n Description: %s\n" %(productCategory, productBrand, productName, productDescription)

	@property
	def name(self):
		return self._name
		
	@property
	def description(self):
		return self._description
		
	@property
	def brand(self):
		return self._brand
		
	@property
	def category(self):
		return self._category
		
	@name.setter
	def name(self, name):
		self._name = name
		
	@description.setter
	def description(self, description):
		self._description = description
	
	@brand.setter
	def set_brand(self, brand):
		self._brand = brand
	
	@category.setter
	def set_category(self, category):
		self._category = category