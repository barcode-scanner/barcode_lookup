#!/usr/bin/env python

from distutils.core import setup

setup(name='Barcode Lookup',
      version='0.1',
      description='Takes a barcode as a parameter and searches the product through engines',
      author='Jean-Christophe Raad & Yves Hulet',
      url='https://gitlab.com/barcode-scanner/barcode_lookup',
     )