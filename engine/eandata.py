import urllib
import json
import sys
import barcode_lookup.domain.product

baseUrl="http://eandata.com/feed/"
urlParameters = {
	'v': 3,
	'keycode': "AB9BCC70585FFEA6",
	'mode': "json",
	'find': None
}

def search(ean13):
	urlParameters['find'] = ean13
	queryString = urllib.urlencode(urlParameters)
	url = baseUrl + '?' + queryString
	response = json.load(urllib.urlopen(url))
	status = response['status']
	if status['code'] == '200':
		eanProduct = response['product']
		return parseProduct(eanProduct)
	else:
		print(status['message'])


def parseProduct(eanProduct):
	attributes = eanProduct.get('attributes','')
	if attributes == "":
		print("This article is unknown! Please try again.")
	else:
		item = product.Product()
		item.name = attributes['product']
		item.description = attributes.get('description','')
		item.brand = attributes.get('brand','')
		item.category = attributes.get('category_text','')
		return item