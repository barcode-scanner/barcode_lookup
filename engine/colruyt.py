import requests
import os
try:
	import configparser
except ImportError:
	import ConfigParser as configparser

settings = configparser.ConfigParser()
settings.read(os.path.join(os.path.abspath(os.path.dirname(__file__)), 'engine.ini'))

username = settings.get('colruyt','Login')
password = settings.get('colruyt','Password')

class ColruytAPI:
	def __init__(self):
		self.token = ""
		self.uri = "https://colruyt.collectandgo.be"
		self.basePath = "/cogomw/rest/nl/4"
		self.headers = {
			"Host": "cogomw.colruyt.be",
			"Proxy-Connection": "keep-alive",
			"Accept-Encoding": "gzip, deflate",
			"Content-Type": "application/x-www-form-urlencoded",
			"Accept-Language": "nl-nl",
			"Accept": "*/*",
			"Connection": "keep-alive",
			"User-Agent": "Collect&Go/3.3.1.11218 CFNetwork/758.1.3 Darwin/15.0.0"
		}
		if username is not None and password is not None:
			self.login(username, password)

	def login(self, username, password):
		path = "/users/authenticate.json"
		body = {"logon_id": username, "password": password}
		response = self.request(path, body)
		
		if response['status']['code'] == 0:
			self.token = response['data']['oAuth']
		else:
			raise ValueError("Login failed: %s" %(response['status']['meaning']))

	def request(self, path, body):
		target = self.uri + self.basePath + path
		response = requests.post(target, headers = self.headers, params = body, auth=(username, password))
		if response.status_code == 200:
			return response.json()
			return response.text
		else:
			raise ValueError("Request failed")

	def logout(self):
		path = "/log_off.json"
		body = {"oAuth": self.token}
		self.request(path, body)
		self.token = ""
		
	def search(self, barcode):
		path = "/articles/search.json"
		body = {"oAuth": self.token, "barcode": barcode}
		response = self.request(path, body)
		
		if response['status']['code'] == 0:
			return response
		else:
			raise ValueError("Research failed: %s" %(response['status']['meaning']))
		
	def add(self, id, qty, wCode):
		path = "/basket/articles/add.json"
		body = {"id": id, "weightCode": wCode, "quantity": qty, "oAuth": self.token}
		response = self.request(path, body)
		
		if response['status']['code'] != 0:
			raise ValueError("Something went wrong could not add your item to your basket: %s" %(response['status']['meaning']))
		
	def show_basket(self):
		path = "/basket/show.json"
		body = {"oAuth": self.token}
		response = self.request(path, body)
		
		if response['status']['code'] == 0:
			return response
		else:
			raise ValueError("Something went wrong when opening your basket: %s" %(response['status']['meaning']))		